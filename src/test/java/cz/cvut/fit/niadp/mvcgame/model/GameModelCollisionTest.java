package cz.cvut.fit.niadp.mvcgame.model;

import cz.cvut.fit.niadp.mvcgame.abstractfactory.GameObjectsFactoryA;
import cz.cvut.fit.niadp.mvcgame.builder.EnemyABuilder;
import cz.cvut.fit.niadp.mvcgame.command.CannonShootCommand;
import cz.cvut.fit.niadp.mvcgame.model.gameObjects.AbsCollision;
import cz.cvut.fit.niadp.mvcgame.model.gameObjects.AbsEnemy;
import cz.cvut.fit.niadp.mvcgame.model.gameObjects.AbsMissile;
import cz.cvut.fit.niadp.mvcgame.strategy.SimpleMovingStrategy;
import mockit.Expectations;
import mockit.Mock;
import mockit.MockUp;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class GameModelCollisionTest {

    private static final int CANNON_TEST_POSITION_X = 100;
    private static final int CANNON_TEST_POSITION_Y = 100;

    @Test
    public void wallCollisionTest() {
        GameObjectsFactoryA.clearInstance();
        GameModel model = new GameModel();

        new Expectations(model) {{
            model.getCannonPosition();
            result = new Position(CANNON_TEST_POSITION_X, CANNON_TEST_POSITION_Y);
            model.generateEnemies();
            result = List.of(
                    (new EnemyABuilder()).setPosition(new Position(300, 100)).setVariant(AbsEnemy.Variant.wall).build(),
                    (new EnemyABuilder()).setPosition(new Position(500, 100)).setVariant(AbsEnemy.Variant.basic).setLives(1).build()
            );
        }};

        new MockUp<SimpleMovingStrategy>() {
            @Mock
            public void updatePosition(AbsMissile missile) {
                missile.move(new Vector(100, 0));
            }
        };

        model.restartGame();

        model.registerCommand(new CannonShootCommand(model));
        model.update();
        model.update();
        model.update();

        // missile hit the wall, it should be destroyed
        Assert.assertEquals(model.getMissiles().size(), 0);
        AbsEnemy e = model.getGameObjects().stream().filter(o -> o instanceof AbsEnemy).map(o -> (AbsEnemy) o).filter(o -> o.getVariant() == AbsEnemy.Variant.basic).findFirst().get();
        AbsEnemy w = model.getGameObjects().stream().filter(o -> o instanceof AbsEnemy).map(o -> (AbsEnemy) o).filter(o -> o.getVariant() == AbsEnemy.Variant.wall).findFirst().get();

        Assert.assertEquals(e.getLives(), 1);
        Assert.assertEquals(w.getLives(), 1);

        Assert.assertEquals(e.getVariant(), AbsEnemy.Variant.basic);
        Assert.assertEquals(w.getVariant(), AbsEnemy.Variant.wall);
    }

    @Test
    public void shootEnemyTest() {
        GameObjectsFactoryA.clearInstance();
        GameModel model = new GameModel();
        new Expectations(model) {{
            model.getCannonPosition();
            result = new Position(CANNON_TEST_POSITION_X, CANNON_TEST_POSITION_Y - 20);
            model.generateEnemies();
            result = List.of((new EnemyABuilder()).setPosition(new Position(500, 100)).setLives(2).build());
        }};

        new MockUp<SimpleMovingStrategy>() {
            @Mock
            public void updatePosition(AbsMissile missile) {
                missile.move(new Vector(100, 0));
            }
        };

        model.restartGame();

        model.registerCommand(new CannonShootCommand(model));
        model.update();

        AbsMissile m = model.getMissiles().get(0);
        AbsEnemy e = model.getGameObjects().stream().filter(o -> o instanceof AbsEnemy).map(o -> (AbsEnemy) o).findFirst().get();

        Assert.assertEquals(CANNON_TEST_POSITION_X + 100, m.getPosition().getX());
        Assert.assertEquals(CANNON_TEST_POSITION_Y, m.getPosition().getY());

        Assert.assertEquals(500, e.getPosition().getX());
        Assert.assertEquals(100, e.getPosition().getY());

        model.update();

        Assert.assertEquals(CANNON_TEST_POSITION_X + 200, m.getPosition().getX());
        Assert.assertEquals(CANNON_TEST_POSITION_Y, m.getPosition().getY());

        model.update();
        model.update();
        model.update();

        Assert.assertEquals(model.getMissiles().size(), 0);
        Assert.assertEquals(e.getLives(), 1);

        model.registerCommand(new CannonShootCommand(model));
        model.update();
        AbsMissile m2 = model.getMissiles().get(0);
        model.update();
        model.update();
        model.update();
        model.update();
        Assert.assertEquals(model.getMissiles().size(), 0);
        Assert.assertEquals(e.getLives(), 0);

        AbsCollision c = model.getGameObjects().stream().filter(o -> o instanceof AbsCollision).map(o -> (AbsCollision) o).findFirst().get();
        Assert.assertEquals(c.getPosition().getX(), 500);
        Assert.assertEquals(c.getPosition().getY(), 100);

        // only collision, cannon, game info
        Assert.assertEquals(model.getGameObjects().size(), 3);
    }

}
