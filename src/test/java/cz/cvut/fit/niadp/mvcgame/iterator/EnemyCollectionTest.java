package cz.cvut.fit.niadp.mvcgame.iterator;

import cz.cvut.fit.niadp.mvcgame.model.Position;
import cz.cvut.fit.niadp.mvcgame.model.gameObjects.AbsEnemy;
import cz.cvut.fit.niadp.mvcgame.model.gameObjects.EnemyA;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.List;

@RunWith(value = Parameterized.class)
public class EnemyCollectionTest {

    public IEnemyCollection collection;

    public EnemyCollectionTest(IEnemyCollection collection) {
        this.collection = collection;
    }

    @Parameterized.Parameters(name = "{index}: Impl Class: {0}")
    public static Object[] data() {
        return new Object[]{
                new EnemyCollection(),
                new OptimizedEnemyCollection()
        };
    }

    @Test
    public void CollectionTest() {
        AbsEnemy e1 = new EnemyA(new Position(0, 0));
        AbsEnemy e2 = new EnemyA(new Position(5, 5));
        AbsEnemy e3 = new EnemyA(new Position(10, 10));
        this.collection.add(e1);
        this.collection.add(e2);
        this.collection.add(e3);

        assert this.collection.size() == 3;

        IEnemyCollection sc1 = this.collection.within(new Position(0, 0), 1);
        assert sc1.size() == 1;
        AbsEnemy ce1 = sc1.iterator().next();
        assert ce1 == e1;

        IEnemyCollection sc2 = this.collection.within(new Position(0, 0), 10);
        assert sc2.size() == 2;

        IEnemyCollection.IEnemyIterator it = sc2.iterator();
        AbsEnemy ce2 = it.next();
        assert ce2 == e1 || ce2 == e2;
        AbsEnemy ce3 = it.next();
        assert ce3 == e1 || ce3 == e2;
        assert ce2 != ce3;
        assert !it.hasNext();

        for (AbsEnemy e : collection) {
            System.out.println(e);
        }

        this.collection.removeAll(List.of(e1, e2, e3));
        assert this.collection.size() == 0;
        this.collection.add(e1);
        this.collection.add(e2);
        this.collection.add(e3);
        this.collection.clear();
        assert this.collection.size() == 0;

        this.collection.add(e1);
        assert this.collection.size() == 1;
        this.collection.add(e2);
        assert this.collection.size() == 2;

        this.collection.removeAll(List.of(e1));
        assert this.collection.size() == 1;
        assert this.collection.iterator().next() == e2;
        this.collection.clear();

        this.collection.add(e1);
        this.collection.add(e2);
        this.collection.add(e3);
        this.collection.removeAll(List.of(e1, e2));
        assert this.collection.size() == 1;
        assert this.collection.iterator().next() == e3;
        this.collection.clear();
        assert this.collection.size() == 0;
    }

}
