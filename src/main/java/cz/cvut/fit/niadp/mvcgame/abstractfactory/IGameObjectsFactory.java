package cz.cvut.fit.niadp.mvcgame.abstractfactory;

import cz.cvut.fit.niadp.mvcgame.model.gameObjects.*;

import java.util.List;

public interface IGameObjectsFactory {
    AbsCannon createCannon();

    AbsMissile createMissile(double initAngle, int initVelocity);

    AbsEnemy createEnemy(int posX, int posY);

    AbsCollision createCollision(AbsMissile missile, AbsEnemy enemy);

    AbsGameInfo createGameInfo();

    AbsEnemy createWall(int posX, int posY);

    AbsLevelOver createLevelOver();

    List<AbsMissile> createMissileShards(AbsMissile missile);
}
