package cz.cvut.fit.niadp.mvcgame.visitor;

import cz.cvut.fit.niadp.mvcgame.model.gameObjects.*;

public interface IGameObjectsVisitor {
    void visitCannon(AbsCannon cannon);

    void visitMissile(AbsMissile missile);

    void visitMissileShard(AbsMissileShard missileShard);

    void visitCollision(AbsCollision collision);

    void visitGameInfo(AbsGameInfo gameInfo);

    void visitEnemy(AbsEnemy enemy);

    void visitLevelOver(AbsLevelOver absLevelOver);
}
