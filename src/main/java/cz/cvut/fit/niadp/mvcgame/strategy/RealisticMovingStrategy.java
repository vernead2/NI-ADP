package cz.cvut.fit.niadp.mvcgame.strategy;

import cz.cvut.fit.niadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.niadp.mvcgame.model.Position;
import cz.cvut.fit.niadp.mvcgame.model.gameObjects.AbsMissile;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class RealisticMovingStrategy implements IMovingStrategy {

    @Override
    public void updatePosition(AbsMissile missile) {
        double initVelocity = missile.getInitVelocity();
        double initAngle = missile.getInitAngle();
        double time = ChronoUnit.MILLIS.between(missile.getCreatedAt(), LocalDateTime.now()) / 1000.0;
        double distance = initVelocity * time * MvcGameConfig.MAX_X / 20;

        // initial missile position
        int iX = missile.getInitPosition().getX();
        int iY = missile.getInitPosition().getY();

        // expected missile position
        int eX = (int) (iX + distance * Math.cos(initAngle));
        int eY = (int) (iY + distance * Math.sin(initAngle) + (MvcGameConfig.GRAVITY * time * time));

        missile.setPosition(new Position(eX, eY));
    }

    @Override
    public String getName() {
        return RealisticMovingStrategy.class.getSimpleName();

    }
}
