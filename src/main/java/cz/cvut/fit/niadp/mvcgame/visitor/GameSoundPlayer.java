package cz.cvut.fit.niadp.mvcgame.visitor;

import cz.cvut.fit.niadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.niadp.mvcgame.observer.Aspect;
import javafx.scene.media.AudioClip;

import java.net.URL;
import java.util.Objects;

public class GameSoundPlayer implements IGameSoundsVisitor {

    private static final String game_started;
    private static final String missile_create;
    private static final String game_exit;

    private static final String enemy_hit1;
    private static final String enemy_hit2;
    private static final String enemy_hit3;

    static {
        ClassLoader classLoader = GameSoundPlayer.class.getClassLoader();
        URL game_started_resource = classLoader.getResource(MvcGameConfig.GAME_STARTED_SOUND_RESOURCE);
        URL missile_create_resource = classLoader.getResource(MvcGameConfig.MISSILE_CREATE_SOUND_RESOURCE);
        URL game_exit_resource = classLoader.getResource(MvcGameConfig.GAME_EXIT_SOUND_RESOURCE);
        URL enemy_hit1_resource = classLoader.getResource(MvcGameConfig.MISSILE_HIT1_SOUND_RESOURCE);
        URL enemy_hit2_resource = classLoader.getResource(MvcGameConfig.MISSILE_HIT2_SOUND_RESOURCE);
        URL enemy_hit3_resource = classLoader.getResource(MvcGameConfig.MISSILE_HIT3_SOUND_RESOURCE);

        game_started = Objects.requireNonNull(game_started_resource).toExternalForm();
        missile_create = Objects.requireNonNull(missile_create_resource).toExternalForm();
        game_exit = Objects.requireNonNull(game_exit_resource).toExternalForm();
        enemy_hit1 = Objects.requireNonNull(enemy_hit1_resource).toExternalForm();
        enemy_hit2 = Objects.requireNonNull(enemy_hit2_resource).toExternalForm();
        enemy_hit3 = Objects.requireNonNull(enemy_hit3_resource).toExternalForm();
    }


    @Override
    public void visitAspect(Aspect aspect) {
        if (MvcGameConfig.SOUND_DISABLED) return;

        if (aspect == Aspect.GAME_STARTED) {
            this.playGameStarted();
        }
        if (aspect == Aspect.MISSILE_CREATE) {
            this.playMissileCreate();
        }
        if (aspect == Aspect.MISSILE_HIT) {
            this.playEnemyHit();
        }
        if (aspect == Aspect.GAME_EXIT) {
            this.playGameExit();
        }
    }

    private void playGameStarted() {
        AudioClip sound = new AudioClip(game_started);
        sound.play();
    }

    private void playMissileCreate() {
        AudioClip sound = new AudioClip(missile_create);
        sound.play();
    }

    private void playGameExit() {
        AudioClip sound = new AudioClip(game_exit);
        sound.play();
    }

    private void playEnemyHit() {
        AudioClip sound = switch ((int) (Math.random() * 3)) {
            case 0 -> new AudioClip(enemy_hit1);
            case 1 -> new AudioClip(enemy_hit2);
            default -> new AudioClip(enemy_hit3);
        };
        sound.play();
    }
}
