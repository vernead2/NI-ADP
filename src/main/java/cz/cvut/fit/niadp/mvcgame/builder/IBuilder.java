package cz.cvut.fit.niadp.mvcgame.builder;

import cz.cvut.fit.niadp.mvcgame.model.Position;
import cz.cvut.fit.niadp.mvcgame.model.gameObjects.AbsEnemy;
import cz.cvut.fit.niadp.mvcgame.model.gameObjects.EnemyA;

public interface IBuilder {
    EnemyABuilder reset();

    EnemyABuilder setPosition(Position position);

    EnemyABuilder setLives(int lives);

    EnemyABuilder setVariant(AbsEnemy.Variant variant);

    EnemyABuilder setAngle(double angle);

    EnemyA build();
}
