package cz.cvut.fit.niadp.mvcgame.abstractfactory;

import cz.cvut.fit.niadp.mvcgame.builder.Director;
import cz.cvut.fit.niadp.mvcgame.builder.EnemyABuilder;
import cz.cvut.fit.niadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.niadp.mvcgame.model.IGameModel;
import cz.cvut.fit.niadp.mvcgame.model.Position;
import cz.cvut.fit.niadp.mvcgame.model.Vector;
import cz.cvut.fit.niadp.mvcgame.model.gameObjects.*;
import cz.cvut.fit.niadp.mvcgame.strategy.SimpleMovingStrategy;

import java.util.ArrayList;
import java.util.List;

public class GameObjectsFactoryA implements IGameObjectsFactory {
    private static IGameObjectsFactory instance;
    private final IGameModel model;

    private GameObjectsFactoryA(IGameModel model) {
        this.model = model;
    }

    public static IGameObjectsFactory getInstance(IGameModel model) {
        if (instance == null) {
            instance = new GameObjectsFactoryA(model);
        }
        return instance;
    }

    public static void clearInstance() {
        // clear instance for testing purposes
        instance = null;
    }

    @Override
    public CannonA createCannon() {
        return new CannonA(new Position(MvcGameConfig.CANNON_POS_X, MvcGameConfig.CANNON_POS_Y), this);
    }

    @Override
    public MissileA createMissile(double initAngle, int initVelocity) {
        Position position = this.model.getCannonPosition().add(new Vector(0, 20));
        return new MissileA(
                position,
                initAngle,
                initVelocity,
                this.model.getMovingStrategy()
        );
    }

    @Override
    public EnemyA createEnemy(int posX, int posY) {
        Director director = new Director(new EnemyABuilder());

        return switch ((int) (Math.random() * 10)) {
            case 0 -> director.buildStrongEnemy(new Position(posX, posY));
            case 1 -> director.buildDamagedEnemy(new Position(posX, posY));
            default -> director.buildSimpleEnemy(new Position(posX, posY));
        };
    }

    @Override
    public EnemyA createWall(int posX, int posY) {
        Director director = new Director(new EnemyABuilder());
        return director.buildWall(new Position(posX, posY));
    }

    @Override
    public CollisionA createCollision(AbsMissile missile, AbsEnemy enemy) {
        return new CollisionA(missile.getPosition());
    }

    @Override
    public AbsGameInfo createGameInfo() {
        return new GameInfoA(new Position(MvcGameConfig.GAME_INFO_POS_X, MvcGameConfig.GAME_INFO_POS_Y));
    }

    @Override
    public AbsLevelOver createLevelOver() {
        LevelOverA levelOver = new LevelOverA(new Position(MvcGameConfig.MAX_X / 2, MvcGameConfig.MAX_Y / 2));
        levelOver.setScore(this.model.getScore());
        levelOver.setBestScore(this.model.getBestScore());
        levelOver.setLevel(this.model.getLevel());
        return levelOver;
    }

    @Override
    public List<AbsMissile> createMissileShards(AbsMissile missile) {
        // create 5 shards with simple moving strategy which shoot in 8 directions
        List<AbsMissile> shards = new ArrayList<>();

        for (int i = 0; i < 8; i++) {

            // generate the shard at position 32 pixels from the missile in the direction it shoots
            Position position =  missile.getPosition().copy().add(new Vector(15, 0).rotate(i * Math.PI / 4));
            MissileShardA shard = new MissileShardA(
                    position,
                    i * Math.PI / 4,
                    missile.getInitVelocity(),
                    new SimpleMovingStrategy()
            );
            shards.add(shard);
        }
        return shards;

    }

}
