package cz.cvut.fit.niadp.mvcgame.iterator;


public interface IterableCollection<T> extends java.lang.Iterable<T> {
    @Override
    Iterator<T> iterator();

}
