package cz.cvut.fit.niadp.mvcgame.iterator;

import cz.cvut.fit.niadp.mvcgame.model.Position;
import cz.cvut.fit.niadp.mvcgame.model.gameObjects.AbsEnemy;

import java.util.List;

public interface IEnemyCollection extends IterableCollection<AbsEnemy> {

    void add(AbsEnemy enemy);

    void removeAll(List<AbsEnemy> enemies);

    void clear();

    int size();

    @Override
    IEnemyIterator iterator();

    IEnemyCollection within(Position target, double radius);

    default void addAll(List<AbsEnemy> absEnemies) {
        for (AbsEnemy enemy : absEnemies) {
            this.add(enemy);
        }
    }

    interface IEnemyIterator extends Iterator<AbsEnemy> {

        @Override
        boolean hasNext();

        @Override
        AbsEnemy next();

    }
}
