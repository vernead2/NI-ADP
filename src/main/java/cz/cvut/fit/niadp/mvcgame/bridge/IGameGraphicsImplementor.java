package cz.cvut.fit.niadp.mvcgame.bridge;

import cz.cvut.fit.niadp.mvcgame.model.Position;

public interface IGameGraphicsImplementor {
    void drawImage(String path, Position position);

    void drawImage(String path, Position position, double angle);

    void drawText(String text, Position position);

    void drawText(String text, Position position, double size);

    void drawLine(Position beginPosition, Position endPosition);

    void drawFilledRectangle(Position leftTop, Position rightBottom);

    void clear();
}
