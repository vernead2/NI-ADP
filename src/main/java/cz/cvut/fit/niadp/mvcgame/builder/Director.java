package cz.cvut.fit.niadp.mvcgame.builder;

import cz.cvut.fit.niadp.mvcgame.model.Position;
import cz.cvut.fit.niadp.mvcgame.model.gameObjects.AbsEnemy;
import cz.cvut.fit.niadp.mvcgame.model.gameObjects.EnemyA;

public class Director {

    private final IBuilder builder;

    public Director(IBuilder builder) {
        this.builder = builder;
    }

    public EnemyA buildSimpleEnemy(Position position) {
        return builder.reset().setPosition(position).setVariant(AbsEnemy.Variant.basic).build();
    }

    public EnemyA buildStrongEnemy(Position position) {
        return builder.reset().setPosition(position).setVariant(AbsEnemy.Variant.strong).setLives(2).build();
    }

    public EnemyA buildDamagedEnemy(Position position) {
        return builder.reset().setPosition(position).setVariant(AbsEnemy.Variant.damaged).setLives(1).build();
    }

    public EnemyA buildWall(Position position) {
        return builder.reset().setPosition(position).setVariant(AbsEnemy.Variant.wall).build();
    }

}
