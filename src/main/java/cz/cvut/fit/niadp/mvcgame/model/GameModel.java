package cz.cvut.fit.niadp.mvcgame.model;

import cz.cvut.fit.niadp.mvcgame.abstractfactory.GameObjectsFactoryA;
import cz.cvut.fit.niadp.mvcgame.abstractfactory.IGameObjectsFactory;
import cz.cvut.fit.niadp.mvcgame.command.AbstractGameCommand;
import cz.cvut.fit.niadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.niadp.mvcgame.iterator.IEnemyCollection;
import cz.cvut.fit.niadp.mvcgame.iterator.OptimizedEnemyCollection;
import cz.cvut.fit.niadp.mvcgame.model.gameObjects.*;
import cz.cvut.fit.niadp.mvcgame.observer.Aspect;
import cz.cvut.fit.niadp.mvcgame.observer.IObserver;
import cz.cvut.fit.niadp.mvcgame.strategy.*;

import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;

import static java.lang.Math.max;
import static java.lang.Thread.sleep;

public class GameModel implements IGameModel {

    private final List<AbsCollision> collisions;
    private final Map<Aspect, Set<IObserver>> observers;
    private final Queue<AbstractGameCommand> unexecutedCommands;
    private final Deque<AbstractGameCommand> executedCommands;
    private final AbsGameInfo gameInfo;
    private final IGameObjectsFactory gameObjectsFactory;
    private final IEnemyCollection enemies;
    private AbsCannon cannon;
    private List<AbsMissile> missiles;
    private AbsLevelOver levelOver;
    private IMovingStrategy movingStrategy;
    private int currentScore;
    private int bestScore;

    private int enemyCount = 0;
    private int level;


    public GameModel() {
        this.gameObjectsFactory = GameObjectsFactoryA.getInstance(this);
        this.cannon = this.gameObjectsFactory.createCannon();
        this.missiles = new ArrayList<>();
        this.enemies = new OptimizedEnemyCollection();
        this.collisions = new ArrayList<>();
        this.movingStrategy = new SimpleMovingStrategy();
        this.unexecutedCommands = new LinkedBlockingQueue<>();
        this.executedCommands = new ArrayDeque<>();
        this.gameInfo = this.gameObjectsFactory.createGameInfo();

        this.observers = new HashMap<>();
        for (Aspect aspect : Aspect.values()) {
            this.observers.put(aspect, new HashSet<>());
        }

        this.restartGame();
    }

    @Override
    public void restartGame() {
        this.cannon.reset();
        this.missiles.clear();
        this.collisions.clear();
        this.enemies.clear();
        this.movingStrategy = new SimpleMovingStrategy();
        this.currentScore = 0;
        this.level = 1;
        this.levelOver = null;
        this.enemies.addAll(this.generateEnemies());
        this.enemyCount = MvcGameConfig.DEFAULT_ENEMY_COUNT + this.level;
        this.updateGameInfo();
        this.notifyObservers(Aspect.GAME_STARTED);
    }

    private void updateGameInfo() {
        this.gameInfo.setMovingStrategyName(this.movingStrategy.getName());
        this.gameInfo.setShootingModeName(this.cannon.getShootingModeName());
        this.gameInfo.setCannonPower(this.cannon.getPower());
        this.gameInfo.setCannonStrength(this.cannon.getStrength());
        this.gameInfo.setEnemiesCount(this.enemyCount);
        this.gameInfo.setScore(this.currentScore);
        this.gameInfo.setBestScore(this.bestScore);
        this.gameInfo.setLevel(this.level);
    }

    public int getScore() {
        return this.currentScore;
    }

    public int getBestScore() {
        return this.bestScore;
    }

    public int getLevel() {
        return this.level;
    }

    private List<AbsEnemy> generateWalls(int posX, int posY) {

        List<AbsEnemy> walls = new ArrayList<>();

        if (Math.random() < MvcGameConfig.WALL_CHANCE) {

            if (Math.random() < MvcGameConfig.DOUBLE_WALL_CHANCE) {
                walls.add(this.gameObjectsFactory.createWall(posX - 40, posY - 16));
                walls.add(this.gameObjectsFactory.createWall(posX - 40, posY + 16));
            } else if (Math.random() < MvcGameConfig.TRIANGLE_WALL_CHANCE) {
                walls.add(this.gameObjectsFactory.createWall(posX - 40, posY + 32));
                walls.add(this.gameObjectsFactory.createWall(posX - 40, posY));
                walls.add(this.gameObjectsFactory.createWall(posX - 40, posY - 32));
            } else {
                walls.add(this.gameObjectsFactory.createWall(posX - 40, posY));
            }

        }
        return walls;
    }

    protected List<AbsEnemy> generateEnemies() {
        List<AbsEnemy> enemies = new ArrayList<>();
        // randomly generate 10 enemies in front of the cannon
        Random rand = new Random();
        for (int i = 0; i < MvcGameConfig.DEFAULT_ENEMY_COUNT + this.level; i++) {

            int[] position = new int[2];
            do {
                position[0] = rand.nextInt(MvcGameConfig.MAX_X - MvcGameConfig.CANNON_POS_X - 300) + MvcGameConfig.CANNON_POS_X + 100;
                position[1] = rand.nextInt(MvcGameConfig.MAX_Y - 200) + 100;
            } while (enemies.stream().anyMatch(enemy -> enemy.getPosition().distanceTo(new Position(position[0], position[1])) < 80));

            int posX = position[0];
            int posY = position[1];

            enemies.add(this.gameObjectsFactory.createEnemy(posX, posY));
            enemies.addAll(this.generateWalls(posX, posY));

        }
        return enemies;
    }

    public void update() {
        this.executeCommands();
        this.moveMissiles();
        this.checkCollisions();
        this.expireCollisions();

        if (this.enemyCount == 0 && this.levelOver == null) {
            this.bestScore = max(this.bestScore, this.currentScore);
            this.levelOver = this.gameObjectsFactory.createLevelOver();
            this.notifyObservers(Aspect.LEVEL_OVER);
            this.level++;
        }
    }

    private void executeCommands() {
        while (!this.unexecutedCommands.isEmpty()) {
            AbstractGameCommand command = this.unexecutedCommands.poll();
            command.doExecute();
            this.executedCommands.add(command);
        }
    }

    @Override
    public void registerCommand(AbstractGameCommand command) {
        this.unexecutedCommands.add(command);
    }

    @Override
    public void undoLastCommand() {
        if (!this.executedCommands.isEmpty()) {
            this.executedCommands.pop().unExecute();
            this.notifyObservers(Aspect.COMMAND_UNDO);
        }
    }

    private void moveMissiles() {
        this.missiles.forEach(AbsMissile::move);
        this.destroyMissiles();
        if (!this.missiles.isEmpty())
            this.notifyObservers(Aspect.MISSILE_MOVE);
    }

    private void destroyMissiles() {
        List<AbsMissile> toRemove = new ArrayList<>();
        for (AbsMissile missile : this.missiles) {
            if (missile.getPosition().getX() > MvcGameConfig.MAX_X || missile.getPosition().getX() < 0) {
                toRemove.add(missile);
            }
            // higher bounding box for realistic shooting strategy
            else if (missile.getPosition().getY() > MvcGameConfig.MAX_Y + 50 || missile.getPosition().getY() < -500) {
                toRemove.add(missile);
            }
        }
        this.missiles.removeAll(toRemove);
        this.gameInfo.setMissilesCount(this.missiles.size());
        if (!toRemove.isEmpty())
            this.notifyObservers(Aspect.MISSILE_DESTROY);
    }

    private void checkCollisions() {
        List<AbsEnemy> enemiesToRemove = new ArrayList<>();
        List<AbsMissile> missilesToRemove = new ArrayList<>();
        List<AbsMissile> missilesToAdd = new ArrayList<>();

        for (AbsMissile missile : this.missiles) {
            for (AbsEnemy enemy : this.enemies.within(missile.getPosition(), MvcGameConfig.COLLISION_DISTANCE)) {
                System.out.println("Collision detected");
                missilesToRemove.add(missile);
                enemy.hit();
                if (enemy.isDestroyed()) {
                    this.enemyCount--;
                    this.currentScore += enemy.getScore();
                    this.collisions.add(this.gameObjectsFactory.createCollision(missile, enemy));
                    enemiesToRemove.add(enemy);

                }

                // 10% chance the missile explodes and sends shards in 8 directions
                if (Math.random() < MvcGameConfig.EXPLOSION_CHANCE) {
                    System.out.println("Missile exploded");
                    missilesToAdd.addAll(this.gameObjectsFactory.createMissileShards(missile));
                }

            }
        }
        this.enemies.removeAll(enemiesToRemove);
        this.missiles.removeAll(missilesToRemove);
        this.missiles.addAll(missilesToAdd);

        this.gameInfo.setScore(this.currentScore);
        this.gameInfo.setBestScore(this.bestScore);
        this.gameInfo.setEnemiesCount(this.enemyCount);
        this.gameInfo.setMissilesCount(this.missiles.size());

        if (!enemiesToRemove.isEmpty())
            this.notifyObservers(Aspect.MISSILE_HIT);

    }

    private void expireCollisions() {
        List<AbsCollision> toRemove = new ArrayList<>();
        for (AbsCollision collision : this.collisions) {
            if (collision.getAge() > MvcGameConfig.COLLISION_EXPIRATION_TIME) {
                toRemove.add(collision);
            }
        }
        this.collisions.removeAll(toRemove);
    }

    public Position getCannonPosition() {
        return this.cannon.getPosition();
    }

    public void moveCannonUp() {
        this.cannon.moveUp();
        this.notifyObservers(Aspect.CANNON_MOVE);
    }

    public void moveCannonDown() {
        this.cannon.moveDown();
        this.notifyObservers(Aspect.CANNON_MOVE);
    }

    public void aimCannonUp() {
        this.cannon.aimUp();
        this.notifyObservers(Aspect.CANNON_MOVE);
    }

    public void aimCannonDown() {
        this.cannon.aimDown();
        this.notifyObservers(Aspect.CANNON_MOVE);
    }

    public void cannonPowerUp() {
        this.cannon.powerUp();
        this.gameInfo.setCannonPower(this.cannon.getPower());
        this.notifyObservers(Aspect.CANNON_MOVE);
    }

    public void cannonPowerDown() {
        this.cannon.powerDown();
        this.gameInfo.setCannonPower(this.cannon.getPower());
        this.notifyObservers(Aspect.CANNON_MOVE);
    }

    public void cannonStrengthUp() {
        this.cannon.strengthUp();
        this.gameInfo.setCannonStrength(this.cannon.getStrength());
        this.notifyObservers(Aspect.CANNON_MOVE);
    }

    public void cannonStrengthDown() {
        this.cannon.strengthDown();
        this.gameInfo.setCannonStrength(this.cannon.getStrength());
        this.notifyObservers(Aspect.CANNON_MOVE);
    }

    public void cannonShoot() {
        if (this.levelOver != null) {
            this.levelOver = null;
            this.enemies.clear();
            this.collisions.clear();
            this.enemies.addAll(this.generateEnemies());
            this.enemyCount = MvcGameConfig.DEFAULT_ENEMY_COUNT + this.level;
            this.notifyObservers(Aspect.LEVEL_OVER);
            return;
        }

        this.missiles.addAll(this.cannon.shoot());
        this.gameInfo.setMissilesCount(this.missiles.size());

        this.currentScore -= switch (this.cannon.getShootingModeName()) {
            case "DoubleShootingMode" -> 2;
            case "DynamicShootingMode" -> this.cannon.getStrength() / 2;
            default -> 1;
        };

        this.gameInfo.setScore(this.currentScore);
        this.notifyObservers(Aspect.MISSILE_CREATE);
    }

    public void exitGame() {
        this.notifyObservers(Aspect.GAME_EXIT);
        try {
            sleep(1000);
        } catch (InterruptedException e) {
            System.err.println("Interrupted while exiting game");
            Thread.currentThread().interrupt();
        }
    }

    public List<AbsMissile> getMissiles() {
        return this.missiles;
    }

    public List<GameObject> getGameObjects() {
        List<GameObject> gameObjectList = new ArrayList<>();
        for (AbsEnemy enemy : this.enemies) {
            gameObjectList.add(enemy);
        }
        gameObjectList.addAll(this.missiles);
        gameObjectList.addAll(this.collisions);
        gameObjectList.add(this.cannon);
        gameObjectList.add(this.gameInfo);
        if (this.levelOver != null)
            gameObjectList.add(this.levelOver);
        return gameObjectList;
    }

    public void tick() {
        this.update();
        this.notifyObservers(Aspect.GAME_TICK);
    }

    public IMovingStrategy getMovingStrategy() {
        return this.movingStrategy;
    }

    public void toggleMovingStrategy() {
        if (this.movingStrategy instanceof SimpleMovingStrategy) {
            this.movingStrategy = new RealisticMovingStrategy();
        } else if (this.movingStrategy instanceof RealisticMovingStrategy) {
            this.movingStrategy = new SpinningMovingStrategy();
        } else if (this.movingStrategy instanceof SpinningMovingStrategy) {
            this.movingStrategy = new ZigZagMovingStrategy();
        } else if (this.movingStrategy instanceof ZigZagMovingStrategy) {
            this.movingStrategy = new SimpleMovingStrategy();
        } else {
            throw new RuntimeException("Unknown moving strategy");
        }
        this.gameInfo.setMovingStrategyName(this.movingStrategy.getName());
        this.notifyObservers(Aspect.CANNON_MOVE);
    }

    public void toggleShootingMode() {
        this.cannon.toggleShootingMode();
        this.gameInfo.setShootingModeName(this.cannon.getShootingModeName());
        this.notifyObservers(Aspect.CANNON_MOVE);
    }

    public Object createMemento() {
        Memento gameModelSnapshot = new Memento();
        gameModelSnapshot.cannon = this.cannon.clone();

        // copy each item of the list
        gameModelSnapshot.enemies = new ArrayList<>();
        for (AbsEnemy enemy : this.enemies) {
            gameModelSnapshot.enemies.add(enemy.clone());
        }

        gameModelSnapshot.missiles = new ArrayList<>();
        for (AbsMissile missile : this.missiles) {
            gameModelSnapshot.missiles.add(missile.clone());
        }

        // collisions are ignored in the memento

        gameModelSnapshot.currentScore = this.currentScore;
        gameModelSnapshot.bestScore = this.bestScore;
        gameModelSnapshot.enemyCount = this.enemyCount;

        return gameModelSnapshot;
    }

    public void setMemento(Object memento) {
        Memento gameModelSnapshot = (Memento) memento;

        // no need to clone, memento is used only once.
        this.cannon = gameModelSnapshot.cannon;
        this.enemies.clear();
        for (AbsEnemy enemy : gameModelSnapshot.enemies) {
            this.enemies.add(enemy);
        }
        this.missiles = gameModelSnapshot.missiles;
        this.currentScore = gameModelSnapshot.currentScore;
        this.bestScore = gameModelSnapshot.bestScore;
        this.enemyCount = gameModelSnapshot.enemyCount;
        this.updateGameInfo();
        this.notifyObservers(Aspect.COMMAND_UNDO);
    }

    @Override
    public void registerObserver(IObserver observer, Aspect aspect) {
        this.observers.get(aspect).add(observer);
    }

    @Override
    public void unregisterObserver(IObserver observer, Aspect aspect) {
        this.observers.get(aspect).remove(observer);
    }

    @Override
    public void notifyObservers(Aspect aspect) {
        for (IObserver iObserver : this.observers.get(aspect)) {
            iObserver.update(aspect);
        }
    }

    private static class Memento {

        public int enemyCount;
        private AbsCannon cannon;
        private List<AbsMissile> missiles;
        private List<AbsEnemy> enemies;
        private List<AbsCollision> collisions;
        private int currentScore;
        private int bestScore;
    }

}
