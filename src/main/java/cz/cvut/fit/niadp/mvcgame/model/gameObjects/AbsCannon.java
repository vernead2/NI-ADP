package cz.cvut.fit.niadp.mvcgame.model.gameObjects;

import cz.cvut.fit.niadp.mvcgame.model.Position;
import cz.cvut.fit.niadp.mvcgame.state.DoubleShootingMode;
import cz.cvut.fit.niadp.mvcgame.state.DynamicShootingMode;
import cz.cvut.fit.niadp.mvcgame.state.IShootingMode;
import cz.cvut.fit.niadp.mvcgame.state.SingleShootingMode;
import cz.cvut.fit.niadp.mvcgame.visitor.IGameObjectsVisitor;

import java.util.List;

public abstract class AbsCannon extends ClonnableGameObject {

    protected static IShootingMode SINGLE_SHOOTING_MODE = new SingleShootingMode();
    protected static IShootingMode DOUBLE_SHOOTING_MODE = new DoubleShootingMode();
    protected static IShootingMode DYNAMIC_SHOOTING_MODE = new DynamicShootingMode();
    protected IShootingMode shootingMode;
    protected int power;
    protected int strength;
    protected Position initPosition;

    public abstract void reset();

    public abstract void moveUp();

    public abstract void moveDown();

    public abstract void aimUp();

    public abstract void aimDown();

    public abstract void powerUp();

    public abstract void powerDown();

    public abstract void strengthUp();

    public abstract void strengthDown();

    public abstract void primitiveShoot();

    public abstract List<AbsMissile> shoot();

    @Override
    public void acceptVisitor(IGameObjectsVisitor visitor) {
        visitor.visitCannon(this);
    }

    public abstract void toggleShootingMode();

    public int getStrength() {
        return this.strength;
    }

    public int getPower() {
        return this.power;
    }

    public String getShootingModeName() {
        return this.shootingMode.getName();
    }

    @Override
    public abstract AbsCannon clone();

}
