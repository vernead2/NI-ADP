package cz.cvut.fit.niadp.mvcgame.strategy;

import cz.cvut.fit.niadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.niadp.mvcgame.model.Position;
import cz.cvut.fit.niadp.mvcgame.model.gameObjects.AbsMissile;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class SimpleMovingStrategy implements IMovingStrategy {
    @Override
    public void updatePosition(AbsMissile missile) {
        double initVelocity = missile.getInitVelocity();
        double initAngle = missile.getInitAngle();
        double time = ChronoUnit.MILLIS.between(missile.getCreatedAt(), LocalDateTime.now()) / 1000.0;

        double distance = initVelocity * time * MvcGameConfig.MAX_X / 30;

        // expected missile position
        int eX = (int) (missile.getInitPosition().getX() + distance * Math.cos(initAngle));
        int eY = (int) (missile.getInitPosition().getY() + distance * Math.sin(initAngle));

        missile.setPosition(new Position(eX, eY));
    }

    @Override
    public String getName() {
        return SimpleMovingStrategy.class.getSimpleName();
    }
}
