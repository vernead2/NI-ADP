package cz.cvut.fit.niadp.mvcgame.model.gameObjects;

import cz.cvut.fit.niadp.mvcgame.model.Position;
import cz.cvut.fit.niadp.mvcgame.visitor.IGameObjectsVisitor;

public abstract class AbsMissileShard extends AbsMissile {

    protected AbsMissileShard(Position initPosition, double initAngle, int initVelocity) {
        super(initPosition, initAngle, initVelocity);
    }

    @Override
    public void acceptVisitor(IGameObjectsVisitor visitor) {
        visitor.visitMissileShard(this);
    }

}
