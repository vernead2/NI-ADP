package cz.cvut.fit.niadp.mvcgame.builder;

import cz.cvut.fit.niadp.mvcgame.model.Position;
import cz.cvut.fit.niadp.mvcgame.model.gameObjects.AbsEnemy;
import cz.cvut.fit.niadp.mvcgame.model.gameObjects.EnemyA;

public class EnemyABuilder implements IBuilder {

    private Position position;
    private AbsEnemy.Variant variant;
    private double angle;
    private int lives;

    public EnemyABuilder() {
        this.reset();
    }

    @Override
    public EnemyABuilder reset() {
        this.position = null;
        this.variant = AbsEnemy.Variant.basic;
        this.angle = 0;
        this.lives = 1;
        return this;
    }

    @Override
    public EnemyABuilder setPosition(Position position) {
        this.position = position;
        return this;
    }

    @Override
    public EnemyABuilder setLives(int lives) {
        this.lives = lives;
        return this;
    }

    @Override
    public EnemyABuilder setVariant(AbsEnemy.Variant variant) {
        this.variant = variant;
        return this;
    }

    @Override
    public EnemyABuilder setAngle(double angle) {
        this.angle = angle;
        return this;
    }


    @Override
    public EnemyA build() {
        if (position == null) throw new IllegalStateException("EnemyA must have x and y set");
        EnemyA enemy = new EnemyA(position, variant, lives);
        enemy.rotate(angle);
        return enemy;
    }
}
