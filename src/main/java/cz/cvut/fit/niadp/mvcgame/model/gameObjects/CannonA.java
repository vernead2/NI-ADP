package cz.cvut.fit.niadp.mvcgame.model.gameObjects;

import cz.cvut.fit.niadp.mvcgame.abstractfactory.IGameObjectsFactory;
import cz.cvut.fit.niadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.niadp.mvcgame.model.Position;
import cz.cvut.fit.niadp.mvcgame.model.Vector;
import cz.cvut.fit.niadp.mvcgame.state.DoubleShootingMode;
import cz.cvut.fit.niadp.mvcgame.state.DynamicShootingMode;
import cz.cvut.fit.niadp.mvcgame.state.SingleShootingMode;

import java.util.ArrayList;
import java.util.List;

public class CannonA extends AbsCannon {

    private final IGameObjectsFactory gameObjectsFactory;
    private final List<AbsMissile> shootingBatch;


    public CannonA(Position initPosition, IGameObjectsFactory gameObjectsFactory) {
        this.initPosition = initPosition;
        this.gameObjectsFactory = gameObjectsFactory;
        this.shootingBatch = new ArrayList<>();

        this.reset();
    }

    @Override
    public void reset() {
        this.position = this.initPosition.copy();
        this.power = MvcGameConfig.INIT_POWER;
        this.angle = MvcGameConfig.INIT_ANGLE;
        this.strength = MvcGameConfig.INIT_STRENGTH;
        this.shootingMode = SINGLE_SHOOTING_MODE;
        this.shootingBatch.clear();
    }

    @Override
    public void moveUp() {
        this.move(new Vector(0, -MvcGameConfig.MOVE_STEP));
    }

    @Override
    public void moveDown() {
        this.move(new Vector(0, MvcGameConfig.MOVE_STEP));
    }

    @Override
    public void aimUp() {
        this.rotate(-MvcGameConfig.ANGLE_STEP);
    }

    @Override
    public void aimDown() {
        this.rotate(+MvcGameConfig.ANGLE_STEP);
    }

    @Override
    public void powerUp() {
        this.power = Math.min(MvcGameConfig.MAX_POWER, this.power + MvcGameConfig.POWER_STEP);
    }

    @Override
    public void powerDown() {
        this.power = Math.max(MvcGameConfig.MIN_POWER, this.power - MvcGameConfig.POWER_STEP);
    }

    @Override
    public void strengthUp() {
        this.strength = Math.min(MvcGameConfig.MAX_CANNON_STRENGTH, this.strength + 1);
    }

    @Override
    public void strengthDown() {
        this.strength = Math.max(1, this.strength - 1);
    }

    @Override
    public void primitiveShoot() {
        this.shootingBatch.add(this.gameObjectsFactory.createMissile(this.angle, this.power));
    }

    @Override
    public List<AbsMissile> shoot() {
        this.shootingBatch.clear();
        this.shootingMode.shoot(this);
        return this.shootingBatch;
    }

    @Override
    public void toggleShootingMode() {
        if (this.shootingMode instanceof SingleShootingMode) {
            this.shootingMode = DOUBLE_SHOOTING_MODE;
        } else if (this.shootingMode instanceof DoubleShootingMode) {
            this.shootingMode = DYNAMIC_SHOOTING_MODE;
        } else if (this.shootingMode instanceof DynamicShootingMode) {
            this.shootingMode = SINGLE_SHOOTING_MODE;
        }
    }

    @Override
    public CannonA clone() {
        CannonA new_cannon = new CannonA(
                this.initPosition.copy(), this.gameObjectsFactory
        );

        new_cannon.position = this.position.copy();
        new_cannon.power = this.power;
        new_cannon.angle = this.angle;
        new_cannon.strength = this.strength;
        new_cannon.shootingMode = this.shootingMode;

        return new_cannon;
    }
}
