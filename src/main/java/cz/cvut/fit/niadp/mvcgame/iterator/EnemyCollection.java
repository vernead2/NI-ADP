package cz.cvut.fit.niadp.mvcgame.iterator;

import cz.cvut.fit.niadp.mvcgame.model.Position;
import cz.cvut.fit.niadp.mvcgame.model.gameObjects.AbsEnemy;

import java.util.ArrayList;
import java.util.List;

public class EnemyCollection implements IEnemyCollection {
    private final List<AbsEnemy> enemies;


    public EnemyCollection() {
        this.enemies = new ArrayList<>();
    }

    public EnemyCollection(List<AbsEnemy> enemies) {
        this.enemies = enemies;
    }

    public String toString() {
        return "EnemyCollection";
    }

    @Override
    public void add(AbsEnemy enemy) {
        this.enemies.add(enemy);
    }

    @Override
    public void removeAll(List<AbsEnemy> enemies) {
        this.enemies.removeAll(enemies);
    }

    @Override
    public void clear() {
        this.enemies.clear();
    }

    @Override
    public int size() {
        return this.enemies.size();
    }

    @Override
    public EnemyIterator iterator() {
        return new EnemyIterator(this);
    }

    @Override
    public EnemyCollection within(Position target, double radius) {
        List<AbsEnemy> result = this.enemies.stream().filter(enemy -> enemy.getPosition().distanceTo(target) < radius).toList();
        return new EnemyCollection(result);
    }

    class EnemyIterator implements IEnemyCollection.IEnemyIterator {

        private int index;

        public EnemyIterator(EnemyCollection collection) {
            this.index = 0;
        }

        @Override
        public boolean hasNext() {
            return this.index < enemies.size();
        }

        @Override
        public AbsEnemy next() {
            if (!this.hasNext()) {
                return null;
            }
            return enemies.get(this.index++);
        }

    }

}
