package cz.cvut.fit.niadp.mvcgame.model.gameObjects;

import cz.cvut.fit.niadp.mvcgame.visitor.IGameObjectsVisitor;

public abstract class AbsGameInfo extends GameObject {

    protected int cannonPower;
    protected int cannonStrength;
    protected String shootingModeName;
    protected String movingStrategyName;
    protected int missilesCount;
    protected int enemiesCount;
    protected int score;
    protected int bestScore;
    protected int level;

    abstract public String getGameInfoText();

    @Override
    public void acceptVisitor(IGameObjectsVisitor visitor) {
        visitor.visitGameInfo(this);
    }

    public void setMovingStrategyName(String name) {
        this.movingStrategyName = name;
    }

    public void setShootingModeName(String name) {
        this.shootingModeName = name;
    }

    public void setCannonStrength(int strength) {
        this.cannonStrength = strength;
    }

    public void setCannonPower(int power) {
        this.cannonPower = power;
    }

    public void setMissilesCount(int missilesCount) {
        this.missilesCount = missilesCount;
    }

    public void setEnemiesCount(int enemiesCount) {
        this.enemiesCount = enemiesCount;
    }

    public void setScore(int currentScore) {
        this.score = currentScore;
    }

    public void setBestScore(int bestScore) {
        this.bestScore = bestScore;
    }


    public void setLevel(int level) {
        this.level = level;
    }
}
