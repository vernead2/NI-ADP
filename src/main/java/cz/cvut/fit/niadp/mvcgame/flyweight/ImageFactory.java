package cz.cvut.fit.niadp.mvcgame.flyweight;

import java.util.HashMap;
import java.util.Map;

public class ImageFactory {

    static Map<String, Image> image_cache = new HashMap<>();

    public static Image getImage(String path) {

        if (image_cache.containsKey(path)) {
            return image_cache.get(path);
        }

        Image image = new Image(path);
        image_cache.put(path, image);
        return image;
    }

}
