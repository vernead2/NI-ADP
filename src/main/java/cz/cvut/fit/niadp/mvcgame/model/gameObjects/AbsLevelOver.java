package cz.cvut.fit.niadp.mvcgame.model.gameObjects;

import cz.cvut.fit.niadp.mvcgame.model.Position;
import cz.cvut.fit.niadp.mvcgame.visitor.IGameObjectsVisitor;

public abstract class AbsLevelOver extends LifetimeLimitedGameObject {
    protected int score;
    protected int bestScore;
    protected int level;

    protected AbsLevelOver(Position position) {
        super(position);
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void setBestScore(int bestScore) {
        this.bestScore = bestScore;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public abstract String getTitle();

    public abstract String getText();

    public abstract Position getTitlePosition();

    public abstract Position getTextPosition();

    @Override
    public void acceptVisitor(IGameObjectsVisitor visitor) {
        visitor.visitLevelOver(this);
    }
}
