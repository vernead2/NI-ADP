package cz.cvut.fit.niadp.mvcgame.iterator;

public interface Iterator<T> extends java.util.Iterator<T> {
    boolean hasNext();

    T next();
}
