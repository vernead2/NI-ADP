package cz.cvut.fit.niadp.mvcgame.flyweight;

public class Image extends javafx.scene.image.Image {

    public Image(String s) {
        super(s);
    }
}
