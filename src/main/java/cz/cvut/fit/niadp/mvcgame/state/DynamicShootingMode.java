package cz.cvut.fit.niadp.mvcgame.state;

import cz.cvut.fit.niadp.mvcgame.model.gameObjects.AbsCannon;

public class DynamicShootingMode implements IShootingMode {

    @Override
    public String getName() {
        return DynamicShootingMode.class.getSimpleName();
    }

    @Override
    public void shoot(AbsCannon cannon) {
        int missile_count = cannon.getStrength();
        double sepAngle = (Math.PI / 3.0 / (missile_count + 1.0));
        cannon.rotate(-Math.PI / 6.0 + sepAngle);
        for (int i = 0; i < missile_count; i++) {
            cannon.primitiveShoot();
            cannon.rotate(sepAngle);
        }
        cannon.rotate(-Math.PI / 6.0);
    }

}
