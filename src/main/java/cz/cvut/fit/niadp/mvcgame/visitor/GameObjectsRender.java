package cz.cvut.fit.niadp.mvcgame.visitor;

import cz.cvut.fit.niadp.mvcgame.bridge.IGameGraphics;
import cz.cvut.fit.niadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.niadp.mvcgame.model.Position;
import cz.cvut.fit.niadp.mvcgame.model.Vector;
import cz.cvut.fit.niadp.mvcgame.model.gameObjects.*;

public class GameObjectsRender implements IGameObjectsVisitor {
    private IGameGraphics gameGraphics;

    public void setGraphicsContext(IGameGraphics gameGraphics) {
        this.gameGraphics = gameGraphics;
    }


    @Override
    public void visitCannon(AbsCannon cannon) {
        this.gameGraphics.drawImage(MvcGameConfig.CANNON_IMAGE_RESOURCE, cannon.getPosition(), cannon.getAngle());
    }

    @Override
    public void visitMissile(AbsMissile missile) {
        this.gameGraphics.drawImage(MvcGameConfig.MISSILE_IMAGE_RESOURCE, missile.getPosition(), missile.getAngle());
    }

    @Override
    public void visitMissileShard(AbsMissileShard missileShard) {
        this.gameGraphics.drawImage(MvcGameConfig.MISSILE_SHARD_IMAGE_RESOURCE, missileShard.getPosition());
    }

    @Override
    public void visitCollision(AbsCollision collision) {
        this.gameGraphics.drawImage(MvcGameConfig.COLLISION_IMAGE_RESOURCE, collision.getPosition());
    }

    @Override
    public void visitGameInfo(AbsGameInfo gameInfo) {
        this.gameGraphics.drawText(gameInfo.getGameInfoText(), gameInfo.getPosition());
    }

    @Override
    public void visitLevelOver(AbsLevelOver absLevelOver) {

        Position top_left = absLevelOver.getPosition().add(new Vector(-50, -100));
        Position bottom_right = absLevelOver.getPosition().add(new Vector(400, 200));

        this.gameGraphics.drawFilledRectangle(new Position(0, 0), new Position(MvcGameConfig.MAX_X, MvcGameConfig.MAX_Y));
        this.gameGraphics.drawRectangle(top_left, bottom_right);
        this.gameGraphics.drawText(absLevelOver.getTitle(), absLevelOver.getTitlePosition(), 50);
        this.gameGraphics.drawText(absLevelOver.getText(), absLevelOver.getTextPosition());
    }

    @Override
    public void visitEnemy(AbsEnemy enemy) {
        String resource = switch (enemy.getVariant()) {
            case basic -> MvcGameConfig.ENEMY1_IMAGE_RESOURCE;
            case strong -> MvcGameConfig.ENEMY2_IMAGE_RESOURCE;
            case damaged -> MvcGameConfig.ENEMY3_IMAGE_RESOURCE;
            case wall -> MvcGameConfig.WALL_IMAGE_RESOURCE;
        };
        this.gameGraphics.drawImage(resource, enemy.getPosition());
    }
}
