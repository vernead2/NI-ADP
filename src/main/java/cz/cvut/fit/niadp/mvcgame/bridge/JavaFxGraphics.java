package cz.cvut.fit.niadp.mvcgame.bridge;

import cz.cvut.fit.niadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.niadp.mvcgame.flyweight.ImageFactory;
import cz.cvut.fit.niadp.mvcgame.model.Position;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.text.Font;
import javafx.scene.transform.Rotate;

public class JavaFxGraphics implements IGameGraphicsImplementor {

    private final GraphicsContext gr;

    public JavaFxGraphics(GraphicsContext gr) {
        this.gr = gr;
    }

    @Override
    public void drawImage(String path, Position position) {
        Image img = ImageFactory.getImage(path);
        this.gr.drawImage(img, position.getX(), position.getY());
    }

    @Override
    public void drawImage(String path, Position position, double angle) {
        Image img = ImageFactory.getImage(path);

        this.gr.save();
        double posX = position.getX() + img.getWidth() / 2;
        double posY = position.getY() + img.getHeight() / 2;

        Rotate r = new Rotate(Math.toDegrees(angle), posX, posY);
        this.gr.setTransform(r.getMxx(), r.getMyx(), r.getMxy(), r.getMyy(), r.getTx(), r.getTy());
        this.gr.drawImage(img, posX, posY);
        this.gr.restore();
    }

    @Override
    public void drawText(String text, Position position) {
        this.gr.fillText(text, position.getX(), position.getY());
    }

    @Override
    public void drawText(String text, Position position, double size) {
        Font f = this.gr.getFont();
        Font new_f = new Font(f.getName(), size);
        this.gr.setFont(new_f);
        this.gr.fillText(text, position.getX(), position.getY());
        this.gr.setFont(f);
    }

    @Override
    public void drawLine(Position beginPosition, Position endPosition) {
        this.gr.strokeLine(beginPosition.getX(), beginPosition.getY(), endPosition.getX(), endPosition.getY());
    }

    @Override
    public void drawFilledRectangle(Position leftTop, Position rightBottom) {
        this.gr.clearRect(leftTop.getX(), leftTop.getY(), rightBottom.getX() - leftTop.getX(), rightBottom.getY() - leftTop.getY());
    }

    @Override
    public void clear() {
        this.gr.clearRect(0, 0, MvcGameConfig.MAX_X, MvcGameConfig.MAX_Y);
    }
}
