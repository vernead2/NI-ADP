package cz.cvut.fit.niadp.mvcgame.model.gameObjects;

import cz.cvut.fit.niadp.mvcgame.model.Position;
import cz.cvut.fit.niadp.mvcgame.visitor.IGameObjectsVisitor;

import java.time.LocalDateTime;

public abstract class AbsMissile extends ClonnableGameObject {

    private final double initAngle;
    private final int initVelocity;
    private final Position initPosition;
    protected LocalDateTime createdAt;

    protected AbsMissile(Position initPosition, double initAngle, int initVelocity) {

        this.position = initPosition.copy();
        this.initAngle = initAngle;
        this.initVelocity = initVelocity;
        this.initPosition = initPosition.copy();

        this.createdAt = LocalDateTime.now();
    }

    public abstract void move();

    public double getInitAngle() {
        return this.initAngle;
    }

    public int getInitVelocity() {
        return this.initVelocity;
    }

    public Position getInitPosition() {
        return this.initPosition;
    }

    @Override
    public void acceptVisitor(IGameObjectsVisitor visitor) {
        visitor.visitMissile(this);
    }

    public LocalDateTime getCreatedAt() {
        return this.createdAt;
    }

    @Override
    public abstract AbsMissile clone();
}
