package cz.cvut.fit.niadp.mvcgame.view;

import cz.cvut.fit.niadp.mvcgame.bridge.IGameGraphics;
import cz.cvut.fit.niadp.mvcgame.controller.GameController;
import cz.cvut.fit.niadp.mvcgame.model.IGameModel;
import cz.cvut.fit.niadp.mvcgame.observer.Aspect;
import cz.cvut.fit.niadp.mvcgame.observer.IObserver;
import cz.cvut.fit.niadp.mvcgame.visitor.GameObjectsRender;
import cz.cvut.fit.niadp.mvcgame.visitor.GameSoundPlayer;

public class GameView implements IObserver {

    private final IGameModel model;
    private final GameController controller;
    private final GameObjectsRender render;
    private final GameSoundPlayer sound;
    private IGameGraphics gameGraphics;

    public GameView(IGameModel model) {
        this.model = model;
        this.controller = new GameController(this.model);

        for (Aspect aspect : Aspect.values()) {
            this.model.registerObserver(this, aspect);
        }
        this.render = new GameObjectsRender();
        this.sound = new GameSoundPlayer();
    }

    public GameController getController() {
        return this.controller;
    }

    private void render(Aspect aspect) {
        // implementing null object for GraphicContext would be too complex.
        // if this.gr usage would be more frequent we could implement NullProxyObject
        if (this.gameGraphics == null) return;

        this.gameGraphics.clear();
        this.model.getGameObjects().forEach(gameObject -> gameObject.acceptVisitor(this.render));
    }

    private void sound(Aspect aspect) {
        this.sound.visitAspect(aspect);
    }

    public void setGraphicsContext(IGameGraphics gameGraphics) {
        this.gameGraphics = gameGraphics;
        this.render.setGraphicsContext(gameGraphics);
        this.render(Aspect.GAME_STARTED);
        this.sound(Aspect.GAME_STARTED);
    }

    @Override
    public void update(Aspect aspect) {
        this.render(aspect);
        this.sound(aspect);
    }
}
