package cz.cvut.fit.niadp.mvcgame.strategy;

import cz.cvut.fit.niadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.niadp.mvcgame.model.Position;
import cz.cvut.fit.niadp.mvcgame.model.Vector;
import cz.cvut.fit.niadp.mvcgame.model.gameObjects.AbsMissile;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class SpinningMovingStrategy implements IMovingStrategy {
    @Override
    public void updatePosition(AbsMissile missile) {
        int initVelocity = missile.getInitVelocity();
        double initAngle = missile.getInitAngle();
        Position initPosition = missile.getInitPosition();

        double time = ChronoUnit.MILLIS.between(missile.getCreatedAt(), LocalDateTime.now()) / 1000.0;

        double angle = time * 5 * Math.PI / 5;
        double distance = time * MvcGameConfig.MAX_X / 10;

        double fX = (initVelocity * distance);
        double fY = 50 * Math.sin(angle);

        int x = (int) (
                initPosition.getX()
                        + fX * Math.cos(initAngle)
                        - fY * Math.sin(initAngle)
        );
        int y = (int) (
                initPosition.getY()
                        + fX * Math.sin(initAngle)
                        + fY * Math.cos(initAngle)
        );

        missile.move(new Vector(x - missile.getPosition().getX(), y - missile.getPosition().getY()));
        missile.rotate(angle - missile.getAngle());
    }

    @Override
    public String getName() {
        return SpinningMovingStrategy.class.getSimpleName();
    }
}
