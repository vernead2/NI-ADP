package cz.cvut.fit.niadp.mvcgame.model.gameObjects;

import cz.cvut.fit.niadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.niadp.mvcgame.model.Position;

public class EnemyA extends AbsEnemy {

    private int initLives = 1;

    public EnemyA(Position initPosition) {
        super();
        this.position = initPosition;
    }

    public EnemyA(Position initPosition, Variant variant) {
        super(variant);
        this.position = initPosition;
    }

    public EnemyA(Position initPosition, Variant variant, int lives) {
        super(variant, lives);
        this.position = initPosition;
        this.initLives = lives;
    }

    @Override
    public int getScore() {
        return initLives * MvcGameConfig.ENEMY_SCORE_MULTIPLIER;
    }

    public void hit() {
        if (variant == Variant.wall) {
            return;
        }

        lives--;
        if (lives == 1) {
            variant = Variant.damaged;
        }
    }

    public boolean isDestroyed() {
        return lives <= 0;
    }

    @Override
    public EnemyA clone() {
        return new EnemyA(this.position.copy(), this.variant);
    }
}
