package cz.cvut.fit.niadp.mvcgame.model.gameObjects;

import cz.cvut.fit.niadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.niadp.mvcgame.model.Position;
import cz.cvut.fit.niadp.mvcgame.model.Vector;

public class LevelOverA extends AbsLevelOver {

    public LevelOverA(Position initPosition) {
        super(initPosition);
    }

    @Override
    public String getTitle() {
        return "Level Finished!";
    }

    @Override
    public String getText() {
        return "Achieved score\t\t\t" + this.score +
                "\nBest score\t\t\t\t" + this.bestScore +
                "\nFinished Level\t\t\t" + this.level +
                "\nPress \"" + MvcGameConfig.EXIT_KEY + "\" to exit the game" +
                "\nPress \"" + MvcGameConfig.RESTART_GAME_KEY + "\" to restart the game" +
                "\nPress \"" + MvcGameConfig.SHOOT_KEY + "\" to continue the game";
    }

    @Override
    public Position getTitlePosition() {
        return this.position;
    }

    @Override
    public Position getTextPosition() {
        return this.position.add(new Vector(0, 50));
    }

}