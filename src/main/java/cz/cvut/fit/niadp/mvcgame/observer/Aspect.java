package cz.cvut.fit.niadp.mvcgame.observer;

public enum Aspect {

    CANNON_MOVE,
    MISSILE_CREATE,
    MISSILE_MOVE,
    MISSILE_DESTROY,

    MISSILE_HIT,

    GAME_STARTED,
    GAME_EXIT,
    COMMAND_UNDO, GAME_TICK, LEVEL_OVER,
}
