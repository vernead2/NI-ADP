package cz.cvut.fit.niadp.mvcgame.model;

public class Vector {
    private int dX = 0;
    private int dY = 0;

    public Vector(int dX, int dY) {
        this.dX = dX;
        this.dY = dY;
    }

    public int getDX() {
        return this.dX;
    }

    public void setDX(int dX) {
        this.dX = dX;
    }

    public int getDY() {
        return this.dY;
    }

    public void setDY(int dY) {
        this.dY = dY;
    }

    public Vector rotate(double angle) {
        double rad = Math.toRadians(angle);
        double cos = Math.cos(rad);
        double sin = Math.sin(rad);
        int x = (int) Math.round(this.dX * cos - this.dY * sin);
        int y = (int) Math.round(this.dX * sin + this.dY * cos);
        return new Vector(x, y);
    }
}
