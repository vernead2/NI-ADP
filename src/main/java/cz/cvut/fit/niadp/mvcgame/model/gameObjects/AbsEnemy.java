package cz.cvut.fit.niadp.mvcgame.model.gameObjects;

import cz.cvut.fit.niadp.mvcgame.visitor.IGameObjectsVisitor;

public abstract class AbsEnemy extends ClonnableGameObject {

    protected Variant variant;
    protected int lives = 1;

    public AbsEnemy() {
        this.variant = Variant.basic;
    }

    public AbsEnemy(Variant variant) {
        this.variant = variant;
    }

    public AbsEnemy(Variant variant, int lives) {
        this.variant = variant;
        this.lives = lives;
    }

    abstract public int getScore();

    abstract public void hit();

    abstract public boolean isDestroyed();

    public int getLives() {
        return lives;
    }

    public Variant getVariant() {
        return variant;
    }

    @Override
    public void acceptVisitor(IGameObjectsVisitor visitor) {
        visitor.visitEnemy(this);
    }

    @Override
    public abstract AbsEnemy clone();

    public enum Variant {
        basic, strong, wall, damaged
    }
}
