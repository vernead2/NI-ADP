package cz.cvut.fit.niadp.mvcgame.config;

public class MvcGameConfig {

    public static final int MAX_X = 1920 / 2;
    public static final int MAX_Y = 1080 / 2;
    public static final int MOVE_STEP = 10;
    public static final int CANNON_POS_X = 200;
    public static final int CANNON_POS_Y = MAX_Y / 2;

    public static final int GAME_INFO_POS_X = 10;
    public static final int GAME_INFO_POS_Y = 10;

    public static final double ANGLE_STEP = Math.PI / 18;
    public static final int POWER_STEP = 1;
    public static final int INIT_POWER = 2;
    public static final double INIT_ANGLE = 0;
    public static final int INIT_STRENGTH = 1;
    public static final double GRAVITY = 9.81;
    public static final int MAX_POWER = 50;
    public static final int MIN_POWER = 1;
    public static final int MAX_CANNON_STRENGTH = 10;
    public static final int COLLISION_EXPIRATION_TIME = 2000;
    public static final double COLLISION_DISTANCE = 32;
    public static final int DEFAULT_ENEMY_COUNT = 5;
    public static double EXPLOSION_CHANCE = 0.1;

    // probability of spawning a wall before enemy
    public static double WALL_CHANCE = 0.25;
    // chance that the spawned wall will be double
    public static final double DOUBLE_WALL_CHANCE = 0.30;
    public static final double TRIANGLE_WALL_CHANCE = 0.15;

    public static final int ENEMY_SCORE_MULTIPLIER = 50;


    public static final String GAME_TITLE = "The NI-ADP MvcGame";

    public static final String UP_KEY = "UP";
    public static final String DOWN_KEY = "DOWN";
    public static final String EXIT_KEY = "ESCAPE";
    public static final String SHOOT_KEY = "SPACE";
    public static final String AIM_UP_KEY = "LEFT";
    public static final String AIM_DOWN_KEY = "RIGHT";
    public static final String POWER_UP_KEY = "D";
    public static final String POWER_DOWN_KEY = "F";
    public static final String MOVING_STRATEGY_KEY = "M";
    public static final String SHOOTING_MODE_KEY = "N";
    public static final String STRENGTH_UP = "C";
    public static final String STRENGTH_DOWN = "V";

    public static final String RESTART_GAME_KEY = "R";
    public static final String UNDO_LAST_COMMAND_KEY = "T";

    public static final String CANNON_IMAGE_RESOURCE = "images/cannon.png";
    public static final String MISSILE_IMAGE_RESOURCE = "images/missile.png";
    public static final String MISSILE_SHARD_IMAGE_RESOURCE = "images/shard.png";
    public static final String COLLISION_IMAGE_RESOURCE = "images/collision.png";
    public static final String ENEMY1_IMAGE_RESOURCE = "images/enemy1.png";
    public static final String ENEMY2_IMAGE_RESOURCE = "images/enemy2.png";
    public static final String ENEMY3_IMAGE_RESOURCE = "images/enemy2WithBlood.png";
    public static final String WALL_IMAGE_RESOURCE = "images/wall.png";

    // all sound effects are kindly borrowed from https://themushroomkingdom.net/media/mk64/wav
    public static final String GAME_STARTED_SOUND_RESOURCE = "sounds/mk64_announcer04-jp.wav";
    public static final String MISSILE_CREATE_SOUND_RESOURCE = "sounds/mk64_boing.wav";
    public static final String GAME_EXIT_SOUND_RESOURCE = "sounds/mk64_peach05.wav";
    public static final String MISSILE_HIT1_SOUND_RESOURCE = "sounds/mk64_wario04.wav";
    public static final String MISSILE_HIT2_SOUND_RESOURCE = "sounds/mk64_wario05.wav";
    public static final String MISSILE_HIT3_SOUND_RESOURCE = "sounds/mk64_toad05.wav";
    // public static final boolean SOUND_DISABLED = false;
    public static boolean SOUND_DISABLED = false;


    private static boolean isJUnitTest() {
        for (StackTraceElement element : Thread.currentThread().getStackTrace()) {
            if (element.getClassName().startsWith("org.junit.")) {
                return true;
            }
        }
        return false;
    }

    static {
        if (isJUnitTest()) {
            MvcGameConfig.SOUND_DISABLED = true;
            MvcGameConfig.EXPLOSION_CHANCE = 0.0;
            MvcGameConfig.WALL_CHANCE = 0.0;
        }
    }

    private MvcGameConfig() {
        throw new IllegalStateException("Utility class");
    }
}