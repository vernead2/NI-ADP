package cz.cvut.fit.niadp.mvcgame.visitor;

import cz.cvut.fit.niadp.mvcgame.observer.Aspect;

public interface IGameSoundsVisitor {
    void visitAspect(Aspect aspect);

}
