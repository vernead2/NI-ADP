package cz.cvut.fit.niadp.mvcgame.model.gameObjects;

public abstract class ClonnableGameObject extends GameObject {

    abstract public ClonnableGameObject clone();
}
