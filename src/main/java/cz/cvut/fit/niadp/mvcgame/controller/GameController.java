package cz.cvut.fit.niadp.mvcgame.controller;

import cz.cvut.fit.niadp.mvcgame.command.*;
import cz.cvut.fit.niadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.niadp.mvcgame.model.IGameModel;

import java.util.List;

public class GameController {

    private final IGameModel model;

    public GameController(IGameModel model) {
        this.model = model;
    }

    public void processPressedKeys(List<String> pressedKeysCodes) {
        for (String code : pressedKeysCodes) {
            switch (code) {
                case MvcGameConfig.UP_KEY:
                    this.model.registerCommand(new MoveCannonUpCommand(this.model));
                    break;
                case MvcGameConfig.DOWN_KEY:
                    this.model.registerCommand(new MoveCannonDownCommand(this.model));
                    break;
                case MvcGameConfig.SHOOT_KEY:
                    this.model.registerCommand(new CannonShootCommand(this.model));
                    break;
                case MvcGameConfig.AIM_UP_KEY:
                    this.model.registerCommand(new AimCannonUpCommand(this.model));
                    break;
                case MvcGameConfig.AIM_DOWN_KEY:
                    this.model.registerCommand(new AimCannonDownCommand(this.model));
                    break;
                case MvcGameConfig.POWER_UP_KEY:
                    this.model.registerCommand(new CannonPowerUpCommand(this.model));
                    break;
                case MvcGameConfig.POWER_DOWN_KEY:
                    this.model.registerCommand(new CannonPowerDownCommand(this.model));
                    break;
                case MvcGameConfig.MOVING_STRATEGY_KEY:
                    this.model.registerCommand(new ToggleMovingStrategyCommand(this.model));
                    break;
                case MvcGameConfig.SHOOTING_MODE_KEY:
                    this.model.registerCommand(new ToggleShootingModeCommand(this.model));
                    break;
                case MvcGameConfig.STRENGTH_UP:
                    this.model.registerCommand(new CannonStrengthUpCommand(this.model));
                    break;
                case MvcGameConfig.STRENGTH_DOWN:
                    this.model.registerCommand(new CannonStrengthDownCommand(this.model));
                    break;
                case MvcGameConfig.UNDO_LAST_COMMAND_KEY:
                    this.model.undoLastCommand();
                    break;
                case MvcGameConfig.RESTART_GAME_KEY:
                    this.model.restartGame();
                    break;
                case MvcGameConfig.EXIT_KEY:
                    this.model.exitGame();
                    System.exit(0);
                    break;
                default:
                    //nothing
            }
        }
        this.model.update();
        pressedKeysCodes.clear();
    }
}
