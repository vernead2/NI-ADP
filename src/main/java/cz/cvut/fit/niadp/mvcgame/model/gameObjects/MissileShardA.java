package cz.cvut.fit.niadp.mvcgame.model.gameObjects;

import cz.cvut.fit.niadp.mvcgame.model.Position;
import cz.cvut.fit.niadp.mvcgame.strategy.IMovingStrategy;

public class MissileShardA extends AbsMissileShard {

    private final IMovingStrategy movingStrategy;

    public MissileShardA(Position initPosition, double initAngle, int initVelocity, IMovingStrategy movingStrategy) {
        super(initPosition, initAngle, initVelocity);
        this.movingStrategy = movingStrategy;

    }

    @Override
    public void move() {
        this.movingStrategy.updatePosition(this);
    }

    @Override
    public MissileA clone() {
        MissileA new_missile = new MissileA(
                this.getInitPosition(), this.getInitAngle(), this.getInitVelocity(), this.movingStrategy
        );

        new_missile.createdAt = this.createdAt;
        new_missile.position = this.position.copy();
        new_missile.angle = this.angle;

        return new_missile;
    }

}
