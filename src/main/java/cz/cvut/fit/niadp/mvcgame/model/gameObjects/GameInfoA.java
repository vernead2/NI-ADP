package cz.cvut.fit.niadp.mvcgame.model.gameObjects;

import cz.cvut.fit.niadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.niadp.mvcgame.model.Position;

public class GameInfoA extends AbsGameInfo {

    public GameInfoA(Position initPosition) {
        this.position = initPosition;
    }

    @Override
    public String getGameInfoText() {
        return "Power\t\t\t\t\t(" + MvcGameConfig.POWER_UP_KEY + "/" + MvcGameConfig.POWER_DOWN_KEY + "):\t" + this.cannonPower + "\n" +
                "Strength\t\t\t\t\t(" + MvcGameConfig.STRENGTH_UP + "/" + MvcGameConfig.STRENGTH_DOWN + "):\t" + this.cannonStrength + "\n" +
                "Moving strategy\t\t\t(" + MvcGameConfig.MOVING_STRATEGY_KEY + "):\t\t" + this.movingStrategyName + "\n" +
                "Shooting mode\t\t\t(" + MvcGameConfig.SHOOTING_MODE_KEY + "):\t\t" + this.shootingModeName + "\n" +
                "Missiles count\t\t\t\t\t\t" + this.missilesCount + "\n" +
                "Enemy count\t\t\t\t\t\t" + this.enemiesCount + "\n" +
                "Restart game\t\t\t(" + MvcGameConfig.RESTART_GAME_KEY + ")" + "\n" +
                "Current level\t\t\t\t\t\t" + this.level + "\n" +
                "Current score\t\t\t\t\t\t" + this.score + "\n" +
                "Best score\t\t\t\t\t\t" + this.bestScore + "\n" +
                "Undo command\t\t\t(" + MvcGameConfig.UNDO_LAST_COMMAND_KEY + ")" + "\n";
    }
}
