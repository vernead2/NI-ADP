package cz.cvut.fit.niadp.mvcgame.model.gameObjects;

import cz.cvut.fit.niadp.mvcgame.model.Position;

public class CollisionA extends AbsCollision {
    public CollisionA(Position initPosition) {
        super(initPosition);
    }
}
