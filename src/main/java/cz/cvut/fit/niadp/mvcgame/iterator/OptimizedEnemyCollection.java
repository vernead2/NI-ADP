package cz.cvut.fit.niadp.mvcgame.iterator;

import cz.cvut.fit.niadp.mvcgame.model.Position;
import cz.cvut.fit.niadp.mvcgame.model.gameObjects.AbsEnemy;

import java.util.List;
import java.util.Stack;

public class OptimizedEnemyCollection implements IEnemyCollection {
    private KDTree tree;

    public OptimizedEnemyCollection() {
        this.tree = new KDTree();
    }

    public String toString() {
        return "OptimizedEnemyCollection";
    }

    @Override
    public EnemyIterator iterator() {
        return new EnemyIterator(this.tree.getRoot());
    }

    @Override
    public IEnemyCollection within(Position target, double radius) {
        List<Object> result = KDTree.searchTree(this.tree.getRoot(), target, radius, 0);
        OptimizedEnemyCollection resultCollection = new OptimizedEnemyCollection();
        for (Object o : result) {
            resultCollection.add((AbsEnemy) o);
        }
        return resultCollection;
    }

    public void add(AbsEnemy enemy) {
        this.tree.insert(enemy.getPosition(), enemy);
    }

    public void removeAll(List<AbsEnemy> enemies) {
        for (AbsEnemy enemy : enemies) {
            this.tree.remove(enemy.getPosition());
        }
    }

    public void clear() {
        this.tree = new KDTree();
    }

    public int size() {
        return this.tree.size();
    }

    public static class EnemyIterator implements IEnemyCollection.IEnemyIterator {

        private final Stack<KDTreeContainer.KDNode> nextNodes;

        public EnemyIterator(KDTreeContainer.KDNode root) {
            this.nextNodes = new Stack<>();
            if (root != null) {
                this.nextNodes.push(root);
            }
        }

        @Override
        public boolean hasNext() {
            return !this.nextNodes.empty();
        }

        @Override
        public AbsEnemy next() {
            if (this.nextNodes.empty()) {
                return null;
            }
            KDTreeContainer.KDNode node = this.nextNodes.pop();
            if (node.right != null) {
                this.nextNodes.push(node.right);
            }
            if (node.left != null) {
                this.nextNodes.push(node.left);
            }
            return (AbsEnemy) node.element;
        }

    }
}
