package cz.cvut.fit.niadp.mvcgame.bridge;

import cz.cvut.fit.niadp.mvcgame.model.Position;

public interface IGameGraphics {
    void drawImage(String path, Position position);

    void drawImage(String path, Position position, double angle);

    void drawText(String text, Position position);

    void drawText(String text, Position position, double Size);

    void drawRectangle(Position leftTop, Position rightBottom);

    void drawFilledRectangle(Position leftTop, Position rightBottom);

    void clear();
}
