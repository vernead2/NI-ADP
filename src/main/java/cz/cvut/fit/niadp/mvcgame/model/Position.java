package cz.cvut.fit.niadp.mvcgame.model;

public class Position {
    private int dimX = 0;
    private int dimY = 0;

    public Position(int posX, int posY) {
        this.dimX = posX;
        this.dimY = posY;
    }

    public int getX() {
        return dimX;
    }

    public void setX(int x) {
        this.dimX = x;
    }

    public int getY() {
        return dimY;
    }

    public void setY(int y) {
        this.dimY = y;
    }

    public Position add(Vector vector) {
        return new Position(this.getX() + vector.getDX(), this.getY() + vector.getDY());
    }

    public Position sub(Vector vector) {
        this.setX(this.getX() - vector.getDX());
        this.setY(this.getY() - vector.getDY());
        return new Position(this.getX() - vector.getDX(), this.getY() - vector.getDY());
    }

    public Position copy() {
        return new Position(this.getX(), this.getY());
    }

    public double distanceTo(Position position) {
        return Math.sqrt(Math.pow(this.getX() - position.getX(), 2) + Math.pow(this.getY() - position.getY(), 2));
    }
}